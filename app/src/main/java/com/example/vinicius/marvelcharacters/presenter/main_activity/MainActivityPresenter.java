package com.example.vinicius.marvelcharacters.presenter.main_activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;


import com.example.vinicius.marvelcharacters.DTO.CharacterDTO;
import com.example.vinicius.marvelcharacters.R;
import com.example.vinicius.marvelcharacters.dependency_injection.PerActivity;
import com.example.vinicius.marvelcharacters.model.api.GetCharactersResponse;
import com.example.vinicius.marvelcharacters.model.data_manager.base.DataManager;
import com.example.vinicius.marvelcharacters.presenter.base.BasePresenter;
import com.example.vinicius.marvelcharacters.utils.NetworkUtils;
import com.example.vinicius.marvelcharacters.view.character_detail_activity.CharacterDetailActivity;
import com.example.vinicius.marvelcharacters.view.main_activity.MainActivity;
import com.example.vinicius.marvelcharacters.view.main_activity.MainActivityMvpView;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinicius on 12/09/17.
 */

@PerActivity
public class MainActivityPresenter<V extends MainActivityMvpView> extends BasePresenter<V>
  implements MainActivityMvpPresenter<V>
{
  @Named("ActivityContext")
  Context context;
  AppCompatActivity activity;
  private Observable<GetCharactersResponse> callCharactersResponse = null;
  //	private Observable<GetMoviesResponse> callTopRatedMoviesResponse = null;
  /*
	 * Coleta todos os subscriptions para fazer unsubscribe depois
	 */
  private CompositeDisposable mCompositeDisposable = new CompositeDisposable();


  @Inject
  public MainActivityPresenter(@Named("ActivityContext") Context context, DataManager dataManager, AppCompatActivity activity)
  {
    super(dataManager);
    this.context = context;
    this.activity = activity;
  }

  @Override
  public void loadCharacters(String limit, String offset)
  {
    getMvpView().showProgressBar();


    if(NetworkUtils.isOnline(context))
    {
      callCharactersResponse = getDataManager().getCharacters(limit, offset);

				/*
				 * subscribeOn(Schedulers.io()) - diz que o Observable(que representa a fonte de dados) fará seu
				 * trabalho fora da thread principal
				 *
				 * observeOn(AndroidSchedulers.mainThread()) - diz que o Observer(aquele que se inscreve em um Observable
				 * para receber os dados) irá receber os dados do Observable na thread principal
				 */
      mCompositeDisposable.add(callCharactersResponse.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<GetCharactersResponse>()
        {
          @Override
          public void accept(GetCharactersResponse getMoviesResponse) throws Exception
          {
            getMvpView().hideProgressBar();
            getMvpView().getCharactersResponse(getMoviesResponse);
          }
        }, new Consumer<Throwable>()
        {
          @Override
          public void accept(Throwable throwable) throws Exception
          {
            getMvpView().hideProgressBar();
            Log.e(((MainActivity) getMvpView()).MAINACTIVITYTAG, throwable.getLocalizedMessage());
          }
        }));
    } else
    {
      getMvpView().hideProgressBar();
      getMvpView().showSnackBar(context.getResources().getString(R.string.no_internet_connection));
    }
  }

  @Override
  public int recyclerViewNumberOfColumns()
  {
    DisplayMetrics displayMetrics = new DisplayMetrics();
    activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    // Esse valor deve ser ajustado de acordo com a largura do poster que será mostrado
    int widthDivider = 300;

    int width = displayMetrics.widthPixels;
    int nColumns = width / widthDivider;
    if(nColumns < 2)
      return 2;
    return nColumns;
  }

  @Override
  public void onRecyclerViewItemClick(CharacterDTO characterDTO, View shredElementTransition)
  {
    Intent i = new Intent(context, CharacterDetailActivity.class);
    i.putExtra(CharacterDTO.PARCELABLE_KEY, characterDTO);

    context.startActivity(i);
    //i.putExtra(CharacterDetailActivity.TRANSITION_NAME, ViewCompat.getTransitionName(shredElementTransition));

//		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
//		{
//			/* Manda para a activity que será chamada a posição e o tamanho iniciais da view que será animada */
//			i.putExtra(CharacterDetailActivity.SHARED_VIEW_INFO_EXTRA, captureValues(shredElementTransition));
//			context.startActivity(i);
//			/*
//			 * Especifica uma animação a ser executada na transição entre duas activities para dispositivos
//			 * com API < 21
//			 */
//			((MainActivity) getMvpView()).overridePendingTransition(0, 0);
//		}
//		else
//		{
//			context.startActivity(i, ActivityOptionsCompat.makeSceneTransitionAnimation((MoviesListActivity) getMvpView(), shredElementTransition, ViewCompat.getTransitionName(shredElementTransition)).toBundle());
//		}
  }

  @Override
  public void onDestroy()
  {
    mCompositeDisposable.clear();
  }
}
