package com.example.vinicius.marvelcharacters.model.data_manager.base;


import com.example.vinicius.marvelcharacters.model.api.base.ApiHelper;

/**
 * Created by vinicius on 11/09/17.
 */

public interface DataManager extends ApiHelper
{
}
