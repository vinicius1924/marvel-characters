package com.example.vinicius.marvelcharacters.DTO;

import java.util.List;

/**
 * Created by vinicius on 23/03/18.
 */

public class DataDTO
{
  private int total;
  private List<CharacterDTO> results;

  public int getTotal()
  {
    return total;
  }

  public List<CharacterDTO> getResults()
  {
    return results;
  }
}
