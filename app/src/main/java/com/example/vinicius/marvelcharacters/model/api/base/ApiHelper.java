package com.example.vinicius.marvelcharacters.model.api.base;

import com.example.vinicius.marvelcharacters.model.api.GetCharactersResponse;

import io.reactivex.Observable;

/**
 * Created by vinicius on 13/09/17.
 */

public interface ApiHelper<T>
{
	Observable<GetCharactersResponse> getCharacters(String limit, String offset);
}
