package com.example.vinicius.marvelcharacters.model.api;

import com.example.vinicius.marvelcharacters.DTO.DataDTO;

import java.util.List;

/**
 * Created by vinicius on 23/03/18.
 */

public class GetCharactersResponse
{
  private int code;
  private String status;
  private DataDTO data;

  public int getCode()
  {
    return code;
  }

  public String getStatus()
  {
    return status;
  }

  public DataDTO getData()
  {
    return data;
  }
}
