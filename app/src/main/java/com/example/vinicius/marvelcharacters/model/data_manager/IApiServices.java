package com.example.vinicius.marvelcharacters.model.data_manager;

import com.example.vinicius.marvelcharacters.model.api.GetCharactersResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by vinicius on 05/02/18.
 */

public interface IApiServices
{
	@GET("characters?ts=1&apikey=f3b647ff997bc5a3da3e68e5a08417d7&hash=7e12765eb2b0139395cff2a569eaee5d")
	Observable<GetCharactersResponse> getCharacters(@Query("limit") String limit, @Query("offset") String offset);
}
