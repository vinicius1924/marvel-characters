package com.example.vinicius.marvelcharacters.DTO;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.List;

/**
 * Created by vinicius on 23/03/18.
 */

public class ComicsDTO implements Parcelable, Cloneable
{
  private List<ItemsDTO> items;

  public ComicsDTO()
  {
  }

  public ComicsDTO(Parcel in)
  {
    readFromParcel(in);
  }

  private void readFromParcel(Parcel in)
  {
    items = in.readArrayList(ItemsDTO.class.getClassLoader());
  }

  public List<ItemsDTO> getItems()
  {
    return items;
  }

  @Override
  public int describeContents()
  {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel parcel, int flags)
  {
    parcel.writeList(items);
  }

  public static final Parcelable.Creator<ComicsDTO> CREATOR = new Parcelable.Creator<ComicsDTO>()
  {
    public ComicsDTO createFromParcel(Parcel in)
    {
      return new ComicsDTO(in);
    }

    public ComicsDTO[] newArray(int size)
    {
      return new ComicsDTO[size];
    }
  };

  public ComicsDTO clone()
  {
    ComicsDTO clone = null;

    try
    {
      clone = (ComicsDTO) super.clone();
    }
    catch(CloneNotSupportedException e)
    {
      Log.e(this.getClass().toString(), this.getClass().toString() + " does not implement Cloneable");
    }

    return clone;
  }
}
