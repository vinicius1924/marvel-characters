package com.example.vinicius.marvelcharacters.view.main_activity;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.vinicius.marvelcharacters.DTO.CharacterDTO;
import com.example.vinicius.marvelcharacters.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by vinicius on 23/03/18.
 */

public class CharactersRecyclerAdapter extends RecyclerView.Adapter<CharactersRecyclerAdapter.CustomViewHolder>
{
  private List<CharacterDTO> charactersList;
  private Context mContext;
  private final ListItemClickListener mOnClickListener;

  public CharactersRecyclerAdapter(Context context, List<CharacterDTO> charactersList, ListItemClickListener mOnClickListener)
  {
    this.charactersList = charactersList;
    this.mContext = context;
    this.mOnClickListener = mOnClickListener;
  }

  public interface ListItemClickListener
  {
    void onListItemClick(int clickedItemIndex, ImageView thumbnailImage);
  }

  @Override
  public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
  {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_characters_layout, parent, false);

    CustomViewHolder viewHolder = new CustomViewHolder(view);

    return viewHolder;
  }

  @Override
  public void onBindViewHolder(final CustomViewHolder customViewHolder, int position)
  {
    CharacterDTO characterDTO = charactersList.get(position);

    customViewHolder.characterName.setText(characterDTO.getName());

    String imageUrl = characterDTO.getThumbnail().getPath() + "/portrait_uncanny." +
      characterDTO.getThumbnail().getExtension();

    Picasso.get().load(imageUrl).into(new com.squareup.picasso.Target()
    {
      @Override
      public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from)
      {
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener()
        {
          @Override
          public void onGenerated(Palette palette)
          {
            customViewHolder.thumbnailImage.setImageBitmap(bitmap);

            ColorDrawable[] colorBackground = {(ColorDrawable) ContextCompat.getDrawable(mContext, R.color.character_name),
              new ColorDrawable(palette.getDarkMutedColor(ContextCompat.getColor(mContext, R.color.character_name)))};
            TransitionDrawable transitionBackground = new TransitionDrawable(colorBackground);

            customViewHolder.contentBackground.setBackground(transitionBackground);

            transitionBackground.startTransition(250);

            customViewHolder.characterName.setTextColor(palette.getLightMutedColor(
              mContext.getResources().getColor(R.color.character_name)));
          }
        });
      }

      @Override
      public void onBitmapFailed(Exception e, Drawable errorDrawable)
      {

      }

      @Override
      public void onPrepareLoad(Drawable placeHolderDrawable)
      {

      }
    });
  }

  @Override
  public int getItemCount()
  {
    return (null != charactersList ? charactersList.size() : 0);
  }


  class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
  {
    private ImageView thumbnailImage;
    private TextView characterName;
    private CardView contentBackground;

    public CustomViewHolder(View itemView)
    {
      super(itemView);

      itemView.setOnClickListener(this);

      this.contentBackground = itemView.findViewById(R.id.contentBackground);
      this.thumbnailImage = itemView.findViewById(R.id.thumbnail);
      this.characterName = itemView.findViewById(R.id.character_name);
    }

    @Override
    public void onClick(View view)
    {
      int position = getAdapterPosition();

      mOnClickListener.onListItemClick(position, thumbnailImage);
    }
  }
}
