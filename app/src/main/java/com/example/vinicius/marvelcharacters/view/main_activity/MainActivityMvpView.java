package com.example.vinicius.marvelcharacters.view.main_activity;


import com.example.vinicius.marvelcharacters.model.api.GetCharactersResponse;
import com.example.vinicius.marvelcharacters.view.base.MvpView;

/**
 * Created by vinicius on 12/09/17.
 */

/*
 * Esta interface deve ser implementada por MovieListActivity e vai conter os métodos
 * que serão chamados pelo presenter para se comunicar com a Activity MoviesListActivity
 */
public interface MainActivityMvpView extends MvpView
{
//	void noFavoritesTextViewVisibility(int visibility);
//	void changeAdapterAccordingToPreference(String preference);
	void getCharactersResponse(GetCharactersResponse response);
//	void getTopRatedMoviesResponse(GetMoviesResponse response);
	void showSnackBar(String message);
	void showProgressBar();
	void hideProgressBar();
}
