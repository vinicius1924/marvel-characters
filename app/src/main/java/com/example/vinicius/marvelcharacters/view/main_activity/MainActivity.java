package com.example.vinicius.marvelcharacters.view.main_activity;

import android.databinding.DataBindingUtil;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.vinicius.marvelcharacters.DTO.CharacterDTO;
import com.example.vinicius.marvelcharacters.R;
import com.example.vinicius.marvelcharacters.databinding.ActivityMainBinding;
import com.example.vinicius.marvelcharacters.model.api.GetCharactersResponse;
import com.example.vinicius.marvelcharacters.presenter.main_activity.MainActivityMvpPresenter;
import com.example.vinicius.marvelcharacters.view.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements MainActivityMvpView, CharactersRecyclerAdapter.ListItemClickListener
{
  private RecyclerView recyclerViewCharacters;
  private CoordinatorLayout coordinatorLayout;
  private Toolbar toolbar;
  private Snackbar snackbar;
  private ProgressBar progressBar;
  private CharactersRecyclerAdapter charactersRecyclerAdapter;
  public static final String MAINACTIVITYTAG = "MainActivity";
  private List<CharacterDTO> charactersList = new ArrayList<CharacterDTO>();
  private int offset = 0;

  @Inject
  MainActivityMvpPresenter<MainActivityMvpView> mPresenter;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    getActivityComponent().inject(this);
    mPresenter.registerView(this);

    recyclerViewCharacters = findViewById(R.id.recycler_view_characters);
    toolbar = findViewById(R.id.toolbar);
    coordinatorLayout = findViewById(R.id.coordinatorLayout);
    progressBar = findViewById(R.id.progressBar);

    setSupportActionBar(toolbar);

    getSupportActionBar().setDisplayShowTitleEnabled(false);

		/*
     * LayoutAnimationController é usado para animar os filhos de um view group. Cada filho do viewgroup
		 * usa a mesma animação, mas para cada um a animação começa em um tempo diferente. A implementação
		 * padrão computa o delay da animação multiplicando uma constante em milisegundos pelo index de cada
		 * filho dentro do view group
		 */
//    LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getApplicationContext(),
//      R.anim.grid_layout_animation_from_bottom);

    recyclerViewCharacters.addOnScrollListener(new EndlessRecyclerOnScrollListener()
    {
      @Override
      public void onLoadMore()
      {
        offset = offset + 20;
        mPresenter.loadCharacters("20", String.valueOf(offset));
      }
    });

		/* Seta o LayoutAnimationController que será utilizado para animar as views filhas desse viewgroup */
    //recyclerViewCharacters.setLayoutAnimation(animation);

    recyclerViewCharacters.setLayoutManager(new GridLayoutManager(this, mPresenter.recyclerViewNumberOfColumns()));

    recyclerViewCharacters.setHasFixedSize(true);

		/* Adapter usado pelo recycler view para mostrar os dados recebidos da internet */
    charactersRecyclerAdapter = new CharactersRecyclerAdapter(this.getApplicationContext(), charactersList, this);
    recyclerViewCharacters.setAdapter(charactersRecyclerAdapter);

    mPresenter.loadCharacters("20", String.valueOf(offset));
  }

  @Override
  public void getCharactersResponse(GetCharactersResponse response)
  {
    charactersList.addAll(response.getData().getResults());
    charactersRecyclerAdapter.notifyDataSetChanged();
    //recyclerViewCharacters.scheduleLayoutAnimation();
  }

  @Override
  public void showSnackBar(String message)
  {
    snackbar = Snackbar.make(coordinatorLayout, message,
      Snackbar.LENGTH_LONG)
      .setAction(getResources().getString(R.string.retry), new View.OnClickListener()
      {
        @Override
        public void onClick(View view)
        {
          snackbar.dismiss();
          mPresenter.loadCharacters("20", String.valueOf(offset));
        }
      });

    snackbar.setDuration(Snackbar.LENGTH_INDEFINITE);
    snackbar.show();
  }

  @Override
  public void showProgressBar()
  {
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideProgressBar()
  {
    progressBar.setVisibility(View.INVISIBLE);
  }

  @Override
  public void onListItemClick(int clickedItemIndex, ImageView thumbnailImage)
  {
    CharacterDTO characterDTO = charactersList.get(clickedItemIndex).clone();
    mPresenter.onRecyclerViewItemClick(characterDTO, thumbnailImage);
    //TODO: implementar a ida para a proxima activity
  }
}
