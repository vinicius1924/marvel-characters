package com.example.vinicius.marvelcharacters;

import android.app.Application;

import com.example.vinicius.marvelcharacters.dependency_injection.components.ApplicationComponent;
import com.example.vinicius.marvelcharacters.dependency_injection.components.DaggerApplicationComponent;
import com.example.vinicius.marvelcharacters.dependency_injection.modules.ApplicationModule;
import com.example.vinicius.marvelcharacters.dependency_injection.modules.NetworkModule;
import com.example.vinicius.marvelcharacters.model.data_manager.base.DataManager;

import javax.inject.Inject;

/**
 * Created by vinicius on 23/03/18.
 */

public class App extends Application
{
  @Inject
  DataManager mDataManager;

  private ApplicationComponent mApplicationComponent;

  @Override
  public void onCreate()
  {
    super.onCreate();

    mApplicationComponent = DaggerApplicationComponent.builder()
      .applicationModule(new ApplicationModule(this))
      .networkModule(new NetworkModule("https://gateway.marvel.com/v1/public/"))
      .build();

		/*
		 * Ao chamar o método inject do component ApplicationComponent o dagger vai procurar nos módulos
		 * de ApplicationComponent, ou nos componentes declarados como dependencies em ApplicationComponent,
		 * um método anotado com @Provides que retorne os tipos anotados com @Inject e vai atribuir
		 * referencias desses objetos a esses campos
		 */
    mApplicationComponent.inject(this);
  }

  public ApplicationComponent getComponent() {
    return mApplicationComponent;
  }

}
