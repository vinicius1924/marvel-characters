package com.example.vinicius.marvelcharacters.view.character_detail_activity;

import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.example.vinicius.marvelcharacters.DTO.CharacterDTO;
import com.example.vinicius.marvelcharacters.DTO.ItemsDTO;
import com.example.vinicius.marvelcharacters.R;
import com.example.vinicius.marvelcharacters.view.base.BaseActivity;
import com.example.vinicius.marvelcharacters.view.main_activity.CharactersRecyclerAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CharacterDetailActivity extends BaseActivity implements CharacterDetailActivityMvpView
{
  private Toolbar toolbar;
  private CollapsingToolbarLayout collapsingToolbarLayout;
  private CharacterDTO characterDTO;
  private int mMutedColor = 0xFF333333;
  private ImageView mPhotoView;
  private RecyclerView recyclerViewComics;
  private ComicsRecyclerAdapter comicsRecyclerAdapter;
  private List<ItemsDTO> comicsList;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_character_detail);

    toolbar = findViewById(R.id.toolbar_article);
    collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
    mPhotoView = findViewById(R.id.photo);
    recyclerViewComics = findViewById(R.id.recycler_view_comics);

    setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayShowTitleEnabled(false);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    Bundle bundle = getIntent().getExtras();
    characterDTO = bundle.getParcelable(CharacterDTO.PARCELABLE_KEY);

    comicsList = characterDTO.getComics().getItems();

    if(toolbar != null)
    {
      toolbar.setTitle(characterDTO.getName());
    }

    String imageUrl = characterDTO.getThumbnail().getPath() + "/portrait_uncanny." +
      characterDTO.getThumbnail().getExtension();

    Picasso.get().load(imageUrl).into(new com.squareup.picasso.Target()
    {
      @Override
      public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from)
      {
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener()
        {
          @Override
          public void onGenerated(Palette palette)
          {
            mMutedColor = palette.getDarkMutedColor(0xFF333333);
            mPhotoView.setImageBitmap(bitmap);

            findViewById(R.id.meta_bar).setBackgroundColor(mMutedColor);

            if(collapsingToolbarLayout != null)
            {
              collapsingToolbarLayout.setContentScrimColor(mMutedColor);
              collapsingToolbarLayout.setStatusBarScrimColor(mMutedColor);
            }
          }
        });
      }

      @Override
      public void onBitmapFailed(Exception e, Drawable errorDrawable)
      {

      }

      @Override
      public void onPrepareLoad(Drawable placeHolderDrawable)
      {

      }
    });

    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    recyclerViewComics.setLayoutManager(layoutManager);

    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerViewComics.getContext(),
      layoutManager.getOrientation());
    recyclerViewComics.addItemDecoration(dividerItemDecoration);

    recyclerViewComics.setHasFixedSize(true);

		/* Adapter usado pelo recycler view para mostrar os dados recebidos da internet */
    comicsRecyclerAdapter = new ComicsRecyclerAdapter(this.getApplicationContext(), comicsList);
    recyclerViewComics.setAdapter(comicsRecyclerAdapter);
  }

  @Override
  public void showSnackBar(String message)
  {

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch(item.getItemId())
    {
      case android.R.id.home:
        onBackPressed();
        return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
