package com.example.vinicius.marvelcharacters.view.character_detail_activity;

import com.example.vinicius.marvelcharacters.view.base.MvpView;

/**
 * Created by vinicius on 18/09/17.
 */

public interface CharacterDetailActivityMvpView extends MvpView
{
//	void floatActionButtonSelected(boolean selected);
//	void progressBarTrailerVisibility(int visibility);
//	void progressBarReviewsVisibility(int visibility);
//	void loadMovieTrailersResponse(GetVideosResponse response);
//	void loadMovieReviewsResponse(GetReviewsResponse response);
	void showSnackBar(String message);
}
