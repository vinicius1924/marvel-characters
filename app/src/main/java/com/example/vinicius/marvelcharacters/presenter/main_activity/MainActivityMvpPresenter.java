package com.example.vinicius.marvelcharacters.presenter.main_activity;

import android.view.View;

import com.example.vinicius.marvelcharacters.DTO.CharacterDTO;
import com.example.vinicius.marvelcharacters.presenter.base.MvpPresenter;
import com.example.vinicius.marvelcharacters.view.main_activity.MainActivityMvpView;

/**
 * Created by vinicius on 12/09/17.
 */

/*
 * Esta interface deve ser implementada por MainActivityPresenter e vai conter os métodos
 * que serão chamados pela view para se comunicar com o Presenter MainActivityPresenter
 *
 * Como MainActivityMvpPresenter extends a classe MvpPresenter e MvpPresenter tem que ser de
 * algum tipo que extenda MvpView, a interface MainActivityMvpPresenter tem que ser de algum
 * tipo que extends MvpView. Nesse caso tem que ser do tipo MoviesListMvpView que é uma
 * interface que extends MvpView
 */
public interface MainActivityMvpPresenter<V extends MainActivityMvpView> extends MvpPresenter<V>
{
	void loadCharacters(String limit, String offset);
	int recyclerViewNumberOfColumns();
//	void onMenuOptionItemSelected(String orderBy);
//	String getMenuOptionItemSelected();
	void onRecyclerViewItemClick(CharacterDTO characterDTO, View shredElementTransition);
//	void onFavoriteRecyclerViewItemClick(MovieDTO movieDTO, View shredElementTransition);
	void onDestroy();
}
