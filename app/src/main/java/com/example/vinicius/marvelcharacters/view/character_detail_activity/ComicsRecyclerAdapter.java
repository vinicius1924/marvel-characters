package com.example.vinicius.marvelcharacters.view.character_detail_activity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vinicius.marvelcharacters.DTO.ItemsDTO;
import com.example.vinicius.marvelcharacters.R;

import java.util.List;

/**
 * Created by vinicius on 23/03/18.
 */

public class ComicsRecyclerAdapter extends RecyclerView.Adapter<ComicsRecyclerAdapter.CustomViewHolder>
{
  private List<ItemsDTO> itemsList;
  private Context mContext;

  public ComicsRecyclerAdapter(Context context, List<ItemsDTO> itemsList)
  {
    this.itemsList = itemsList;
    this.mContext = context;
  }

  @Override
  public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
  {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_comics_layout, parent, false);

    CustomViewHolder viewHolder = new CustomViewHolder(view);

    return viewHolder;
  }

  @Override
  public void onBindViewHolder(final CustomViewHolder customViewHolder, int position)
  {
    ItemsDTO itemsDTO = itemsList.get(position);

    customViewHolder.comicName.setText(itemsDTO.getName());
  }

  @Override
  public int getItemCount()
  {
    return (null != itemsList ? itemsList.size() : 0);
  }


  class CustomViewHolder extends RecyclerView.ViewHolder
  {
    private TextView comicName;

    public CustomViewHolder(View itemView)
    {
      super(itemView);

      this.comicName = itemView.findViewById(R.id.comic_name);
    }
  }
}
