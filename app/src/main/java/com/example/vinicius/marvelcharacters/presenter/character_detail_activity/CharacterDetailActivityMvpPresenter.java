package com.example.vinicius.marvelcharacters.presenter.character_detail_activity;

import android.database.Cursor;

import com.example.vinicius.marvelcharacters.presenter.base.MvpPresenter;
import com.example.vinicius.marvelcharacters.view.character_detail_activity.CharacterDetailActivityMvpView;

/**
 * Created by vinicius on 18/09/17.
 */

public interface CharacterDetailActivityMvpPresenter<V extends CharacterDetailActivityMvpView> extends MvpPresenter<V>
{
	void onCreate();
//	void loadMovieTrailers(long id);
//	void loadMovieReviews(long id);
//	boolean isTrailersRequestsCanceled();
//	boolean isReviewsRequestsCanceled();
//	void deleteMovieFromLocalDatabase(long id, String posterPath);
//	void addMovieToLocalDatabase(MovieDTO movieDTO);
//	Cursor findMovieById(long id);
	void onDestroy();
}
